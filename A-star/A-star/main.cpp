#include "Map.h"
#include "SFML\Graphics.hpp"
#include "SFML\OpenGL.hpp"
#include "algorithms.h"
#include <thread>
#include <chrono>



class Program 
{
	sf::RenderWindow window;
	sf::Event event;
	Map *map;
	
	std::thread rt1;
public:
	Program() 
	{
		window.create(sf::VideoMode(500, 600), "main");
		window.setFramerateLimit(60);
		this->map = new Map(MAPSIZE);
	}

	Map getMap() 
	{
		return *map;
	}

	void updateMap() 
	{
		for (int i = 0; i < map->size; i++)
		{
			for (int j = 0; j < map->size; j++)
			{
				map->nodes[i][j]->isPathPart = false;
				map->nodes[i][j]->isTested = false;
				map->nodes[i][j]->value = INF;
				map->nodes[map->startPoint[0]][map->startPoint[1]]->value = 0;
			}
		}
		this->map->setValues();
		algorithm(*map);

	}

	void render()
	{
		drawMap();
	}

	int drawMap()
	{
		sf::RectangleShape tile;
		tile.setSize(sf::Vector2f(window.getSize().x / map->size, window.getSize().x / map->size));
		window.clear();
		for (int i = 0; i < map->size; i++)
		{
			for (int j = 0; j < map->size; j++)
			{
				tile.setPosition(window.getSize().x / map->size * j, window.getSize().x / map->size * i);
				if (map->nodes[i][j]->isObsticle)
				{
					tile.setFillColor(sf::Color::Red);
					window.draw(tile);
				}
				else if (map->nodes[i][j]->isPathPart)
				{
					tile.setFillColor(sf::Color::Blue);
					window.draw(tile);
				}
				else
				{
					tile.setFillColor(sf::Color::Black);
					window.draw(tile);
				}
			}
		}
		tile.setPosition(window.getSize().x / map->size * map->finishPoint[1], window.getSize().x / map->size * map->finishPoint[0]);
		tile.setFillColor(sf::Color::Green);
		window.draw(tile);
		tile.setPosition(window.getSize().x / map->size * map->startPoint[1], window.getSize().x / map->size * map->startPoint[0]);
		tile.setFillColor(sf::Color::Yellow);
		window.draw(tile);

		window.display();
		return 0;
	}

	int drawMap(Map map)
	{
		sf::RectangleShape tile;
		tile.setSize(sf::Vector2f(window.getSize().x / map.size, window.getSize().x / map.size));
		window.clear();
		for (int i = 0; i < map.size; i++)
		{
			for (int j = 0; j < map.size; j++)
			{
				tile.setPosition(window.getSize().x / map.size * j, window.getSize().x / map.size * i);
				if (map.nodes[i][j]->isObsticle)
				{
					tile.setFillColor(sf::Color::Red);
					window.draw(tile);
				}
				else if (map.nodes[i][j]->isPathPart)
				{
					tile.setFillColor(sf::Color::Blue);
					window.draw(tile);
				}
				else
				{
					tile.setFillColor(sf::Color::Black);
					window.draw(tile);
				}
			}
		}
		tile.setPosition(window.getSize().x / map.size * map.finishPoint[1], window.getSize().x / map.size * map.finishPoint[0]);
		tile.setFillColor(sf::Color::Green);
		window.draw(tile);
		tile.setPosition(window.getSize().x / map.size * map.startPoint[1], window.getSize().x / map.size * map.startPoint[0]);
		tile.setFillColor(sf::Color::Yellow);
		window.draw(tile);

		window.display();
		return 0;
	}

	void handleEvents()
	{
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;

			case sf::Event::MouseButtonPressed:
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
					sf::Vector2i mPos = sf::Mouse::getPosition(window);
					int mPosx = sf::Mouse::getPosition(window).x / map->size;
					int mPosy = sf::Mouse::getPosition(window).y / map->size;
					if( ( mPos.x / window.getSize().x < 1 ) && ( mPos.y / window.getSize().x < 1 ) )
					{
						if (!map->nodes[mPos.y / (window.getSize().x / map->size)][mPos.x / (window.getSize().x / map->size)]->isStart &&
							!map->nodes[mPos.y / (window.getSize().x / map->size)][mPos.x / (window.getSize().x / map->size)]->isFinish)
						{
							map->nodes[mPos.y / (window.getSize().x / map->size)][mPos.x / (window.getSize().x / map->size)]->isObsticle =
								!map->nodes[mPos.y / (window.getSize().x / map->size)][mPos.x / (window.getSize().x / map->size)]->isObsticle;

							map->tiles[mPos.y / (window.getSize().x / map->size)][mPos.x / (window.getSize().x / map->size)] = 
							map->nodes[mPos.y / (window.getSize().x / map->size)][mPos.x / (window.getSize().x / map->size)]->isObsticle;
						}
					}
				}
				break;

			case sf::Event::KeyPressed:
				if (event.key.code == sf::Keyboard::Q)
					window.close();
				break;

			default: break;
			}
		}
	}

	void loop()
	{
		while (window.isOpen()) 
		{
			render();
			handleEvents();
			updateMap();
		}
	}
};
//bool isWorking
//void fun(const Map map)
//{
//	while(true)
//	cout << map.nodes[0][0]->isObsticle << endl;
//}



int main()
{
	Map map1(10);
	Map map2(map1);

	Program prog;
	//std::thread t1(fun, map);

	prog.loop();

	//t1.join();
	//prog.loop();
	
}

